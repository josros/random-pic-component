const deps = require("./package.json").dependencies;
const { ModuleFederationPlugin } = require("webpack").container;
const path = require("path");

let config = {
  entry: "./src/index",
  devServer: {
    contentBase: __dirname,
    publicPath: '/',
    compress: true,
    port: 3003
  },
  output: {
    filename: '[name].js',
    path: path.join(__dirname, 'dist')
  },
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      }
    ]
  },
  plugins: [
    new ModuleFederationPlugin({
      name: "randomPic",
      library: { type: "var", name: "randomPic" },
      filename: "randomPicEntry.js",
      exposes: {
        "./component": "./src/random-pic/random-pic",
      },
      // shared must be defined in consumer as well as in the component itself
      // otherwise chunk is loaded twice from consumer and component
      shared: {
        "@material/mwc-button": {
          requiredVersion: deps["@material/mwc-button"] // ^0.17.2
        }
      }
    })
  ],
}
module.exports = (env, argv) => {
  if (argv.mode === 'development') {
    config.output.publicPath = 'http://localhost:3003/';
  }

  if (argv.mode === 'production') {
    config.output.publicPath = "https://josros.gitlab.io/random-pic-component/"
  }

  return config;
};
