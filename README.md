# random-pic-component

A simple [Web Component](https://github.com/w3c/webcomponents) that shows random pictures loaded from [picsum](https://picsum.photos). Pictures are updated after a certain amount of time (`update-freq-ms`). Here is how the custom-element looks like:

```html
<random-pic update-freq-ms="15000" image-length-px="300"></random-pic>
```

and here is a [Demo](https://josros.gitlab.io/random-pic-component/).

Note that the purpose of this repository is the provisioning of a [Web Component](https://github.com/w3c/webcomponents) using [webpack 5 module federation](https://webpack.js.org/concepts/module-federation/) for demonstration reasons. The component is not meant to be used elsewhere.
To keep it simple, the component is implemented as a native [Web Component](https://github.com/w3c/webcomponents) without further framework support.

## Executive summary

### Local execution

```bash
# install dependencies
npm i

# run and view component locally
npm run start
open http://localhost:3003/

# serve component via webpack 5 module federation
# serves component from http://localhost:3003/randomPicEntry.js
npm run serve:locally

```