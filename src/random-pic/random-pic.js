import axios from 'axios';
import "@material/mwc-button";

const template = document.createElement('template');
template.innerHTML = `
  <style>
    #img-container {
      position: relative;
      display: inline-block;
      padding: 10px;
      padding-bottom: 30px;
      background: grey;
    }
    #img-details {
      position: absolute;
      bottom: 5;
    }
    #img-actions {
      margin-bottom: 10px;
    }
    mwc-button {
      --mdc-theme-primary: black;
      --mdc-theme-on-primary: white;
    }
  </style>
  <div id="img-container">
    <div id="img-actions">
      <mwc-button id="btn-like" dense unelevated label="Like"></mwc-button>
    </div>
    <div id="img-details"></div>
  </div>
`

class RandomPic extends HTMLElement {  
  static get defaultFreq() {
    return 5000;
  }
  static get defaultLength() {
    return 300;
  }
  
  static get observedAttributes() {
    return [ 'update-freq-ms', 'image-length-px' ];
  }

  set updateFreqMs(value) {
    this.setAttribute("update-freq-ms", value);
  }

  get updateFreqMs() {
    return this.getAttribute("update-freq-ms") || `${RandomPic.defaultFreq}`
  }

  set imageLengthPx(value) {
    this.setAttribute("image-length-px", value);
  }

  get imageLengthPx() {
    return this.getAttribute("image-length-px")  || `${RandomPic.defaultLength}`
  }
  
  constructor() {
    super();
    const templateContent = template.content;

    this.attachShadow({mode: 'open'}).appendChild(
      templateContent.cloneNode(true)
    );
  }

  connectedCallback() {
    console.log('RandomPic added to DOM');
    this.registerLikeBtnListener();
    this.element = this.shadowRoot.querySelector('#img-container');
    this.imageUrl = `https://picsum.photos/${this.imageLengthPx}/${this.imageLengthPx}?`;
    this.image = document.createElement("img");
    this.element.appendChild(this.image);
    this.loadImage();
    this.redefineTimeout(this.updateFreqMs)
  }

  registerLikeBtnListener() {
    this.shadowRoot.querySelector('#btn-like').addEventListener('click', (event) => {
      console.log("Liked", this.image.src);
      alert("You seem to like the picture. Thank you!");
    })
  }

  disconnectedCallback() {
    console.log('RandomPic removed from DOM');
    clearTimeout();
  }

  attributeChangedCallback(name, oldVal, newVal) {
    if (oldVal !== newVal) {
       console.log(`${name} changed from ${oldVal} to ${newVal}`)
       switch(name) {
        case 'update-freq-ms':
            this.updateFreqMsUpdated(newVal);
            break;
        case 'image-length-px':
            this.imageLengthPxUpdated(newVal);
            break;
        }
    }
  }

  updateFreqMsUpdated(newVal) {
    try {
      this.assertInteger(newVal);
      this.updateFreqMs = newVal;
      this.redefineTimeout(this.updateFreqMs);
    } catch(err) {
      throw Error("update-freq-ms must be a number")
    }
    
  }

  imageLengthPxUpdated(newVal) {
    try {
      this.assertInteger(newVal);
      this.imageLengthPx = newVal;
      this.imageUrl = `https://picsum.photos/${this.imageLengthPx}/${this.imageLengthPx}?`;
    } catch(err) {
      throw Error("image-length-px must be a number")
    }
  }

  assertInteger(value) {
    parseInt(value) // should fail if nan
  }

  redefineTimeout(timeout) {
    this.clearTimeout();
    this.timeout = setTimeout(() => {
      this.timeoutCallback(timeout);
    }, timeout);
  }

  timeoutCallback(timeout) {
    this.loadImage();
    this.redefineTimeout(timeout);
  }

  loadImage() {
    this.updateDetails("");
    axios.get(this.imageUrl)
      .then((response) => {
        this.image.src = response.request?.responseURL;
        this.loadImageDetails(response.headers["picsum-id"]);
      })
      .catch((error) => {
        console.error(error);
      })
  }

  loadImageDetails(imageId) {
    axios.get(`https://picsum.photos/id/${imageId}/info`)
      .then((response) => {
        this.updateDetails(response.data.author)
      })
      .catch((error) => {
        console.error(error);
      })
  }

  updateDetails(text) {
    this.detailsElement = this.shadowRoot.querySelector('#img-details');
    this.detailsElement.innerHTML = text;
  }

  clearTimeout() {
    if(this.timeout) {
      clearTimeout(this.timeout);
    }
  }
}

const elementName = 'random-pic';
window.customElements.define(elementName, RandomPic);

export { elementName };